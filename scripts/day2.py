import pathlib
import time

import numpy as np

CWD = pathlib.Path(__file__).parent
DATAFILE = CWD.parent/'data'/f'{pathlib.Path(__file__).stem}.txt'

with open(DATAFILE, encoding='utf8') as rd:
    games = rd.read().split('\n')

# Star 1
nmax = dict(
    red = 12,
    green = 13,
    blue = 14
    )
t0 = time.time()
games_ok = []
for igame, game in enumerate(games, 1):
    draws = game.split(':')[1].split(';')
    invalid = False
    for draw in draws:
        draw = draw.strip().split(', ')
        for cubes in draw:
            ncubes,color = cubes.split(' ')
            ncubes = int(ncubes.strip())
            ncubesmax = nmax[color.strip()]
            if ncubes > ncubesmax:
                invalid = True
                break
        if invalid:
            break
    else:
        games_ok.append(igame)
print(f'Sum of games: {sum(games_ok)}')
print(f'(achieved in {time.time()-t0:.2f}s)')


# # Star2
t0 = time.time()
ngames = len(games)
powers = np.zeros((ngames,3), dtype=int)
for game,pows in zip(games,powers):
    draws = game.split(':')[1].split(';')
    nmax = dict(
        red = 0,
        green = 0,
        blue = 0
        )
    for draw in draws:
        draw = draw.strip().split(', ')
        for cubes in draw:
            ncubes,color = cubes.split(' ')
            ncubes = int(ncubes.strip())
            nmax[color] = max(nmax[color], ncubes)
    pows[:] = tuple(nmax.values())
powers = np.prod(powers, axis=1)
print(f'Sum of powers: {sum(powers)}')
print(f'(achieved in {time.time()-t0:.2f}s)')