import pathlib
import time

CWD = pathlib.Path(__file__).parent
DATAFILE = CWD.parent/'data'/f'{pathlib.Path(__file__).stem}.txt'

with open(DATAFILE, encoding='utf8') as rd:
    calibration = rd.read().split('\n')

# Star 1
t0 = time.time()
integers = [
    [digit for digit in chars if digit.isdigit()] for chars in calibration
    ]
integers = [int(digits[0]+digits[-1]) for digits in integers if digits]
print(f'Total calibration: {sum(integers)}')
print(f'(achieved in {time.time()-t0:.2f}s)')


# Star2
t0 = time.time()
candidates = {
    '1': '1',
    '2': '2',
    '3': '3',
    '4': '4',
    '5': '5',
    '6': '6',
    '7': '7',
    '8': '8',
    '9': '9',
    'one': '1',
    'two': '2',
    'three': '3',
    'four': '4',
    'five': '5',
    'six': '6',
    'seven': '7',
    'eight': '8',
    'nine': '9',
    }
integers = []
for chars in calibration:
    nchars = len(chars)
    digits = []
    latest_digit = None
    for ichar in range(nchars):
        for candidate,digit in candidates.items():
            if chars[ichar:].startswith(candidate):
                if latest_digit is None:
                    digits.append(digit)
                latest_digit = digit
                break
    digits.append(latest_digit)
    integers.append(int(digits[0]+digits[-1]))
         
print(f'Total calibration: {sum(integers)}')
print(f'(achieved in {time.time()-t0:.2f}s)')